<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Auth::routes();

Route::get('/', 'ContactController@index')->name('home');
Route::get('/contacts/{contact?}', 'ContactController@contact')->name('contact');
Route::get('/contacts/delete/{contact?}', 'ContactController@delete')->name('delete');
Route::post('/contacts/store', 'ContactController@store')->name('contact_store');

Route::get('/contacts/phones/{contact?}', 'ContactController@phone')->name('phone');
Route::post('/phones/store', 'ContactController@phone_store')->name('phone_store');
Route::get('/phones/list/{contact?}', 'ContactController@phone_list')->name('phone_list');

Route::get('/contacts/addresses/{contact?}', 'ContactController@address')->name('address');
Route::post('/addresses/store', 'ContactController@address_store')->name('address_store');
Route::get('/addresses/list/{contact?}', 'ContactController@address_list')->name('address_list');
