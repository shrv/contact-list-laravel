<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Address;
use App\Contact;
use App\Phone;
use Validator;

class ContactController extends Controller
{

    public function index(){
        $contacts = Contact::all();
        foreach($contacts as $contact){
            $contact->phones;
            $contact->addresses;
        }
        return view('home',compact('contacts'));
    }

    public function contact(Request $request){
        $contact = $request->contact? Contact::find($request->contact) : null;
        return view('contact-create',compact('contact'));
    }

    public function delete(Request $request){
        $contact = Contact::find($request->contact);
        if($contact->delete()){
            return back()->with('status',"Contact Deleted Successfully");
        }
        return back()->with('failed',"Contact Deletion Failed");
    }

    public function store(Request $request) {
        $mode_update = $request->id ? true : false;

        if ($mode_update) {
            $contact = Contact::find($request->id);
        } else {
            $contact = new Contact;
        }

        $validator = Validator::make($request->all(), [
            'first_name'    => ['required', 'string', 'max:255'],
            'last_name'     => ['required', 'string', 'max:255'],
            'email'         => ['required', 'string', 'max:255'],
            'date_of_birth' => ['required', 'string', 'max:255'],
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $contact->first_name = $request->input('first_name');
        $contact->last_name = $request->input('last_name');
        $contact->email = $request->input('email');
        $contact->date_of_birth = $request->input('date_of_birth');

        if($contact->save()){
            if($mode_update){
                return back()->with('status', 'contact modified successfully');
            }
            return back()->with('status', 'contact created successfully');
        }else{
            return back()->with('failed', 'contact creation failed');
        }
    }

    public function phone(Request $request){
        $contact = $request->contact? Contact::find($request->contact) : null;
        return view('phone-create',compact('contact'));
    }

    public function phone_store(Request $request) {
        $mode_update = $request->id ? true : false;

        if ($mode_update) {
            $phone = Phone::find($request->id);
        } else {
            $phone = new Phone;
        }

        $validator = Validator::make($request->all(), [
            'contact_id'    => ['required', 'string', 'max:255'],
            'name'          => ['required', 'string', 'max:255'],
            'number'        => ['required', 'string', 'max:255'],
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $phone->contact_id = $request->input('contact_id');
        $phone->name = $request->input('name');
        $phone->number = $request->input('number');

        if($phone->save()){
            if($mode_update){
                return back()->with('status', 'phone modified successfully');
            }
            return back()->with('status', 'phone created successfully');
        }else{
            return back()->with('failed', 'phone creation failed');
        }
    }
    
    public function phone_list(Request $request){
        $contact = Contact::find($request->contact);
        $contact->phones;
        return view('phone-list',compact('contact'));       
    }

    public function address(Request $request){
        $contact = $request->contact? Contact::find($request->contact) : null;
        return view('address-create',compact('contact'));
    }

    public function address_store(Request $request) {
        $mode_update = $request->id ? true : false;

        if ($mode_update) {
            $address = Address::find($request->id);
        } else {
            $address = new Address;
        }
        $validator = Validator::make($request->all(), [
            'contact_id'=> ['required', 'string', 'max:255'],
            'name'      => ['required', 'string', 'max:255'],
            'street'    => ['required', 'string', 'max:255'],
            'city'      => ['required', 'string', 'max:255'],
            'state'     => ['required', 'string', 'max:255'],
            'zip_po'    => ['required', 'string', 'max:255'],
            'country'   => ['required', 'string', 'max:255'],
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $address->contact_id = $request->input('contact_id');
        $address->name = $request->input('name');
        $address->street = $request->input('street');
        $address->city = $request->input('city');
        $address->state = $request->input('state');
        $address->zip_po = $request->input('zip_po');
        $address->country = $request->input('country');

        if($address->save()){
            if($mode_update){
                return back()->with('status', 'address modified successfully');
            }
            return back()->with('status', 'address created successfully');
        }else{
            return back()->with('failed', 'address creation failed');
        }
    }

    public function address_list(Request $request){
        $contact = Contact::find($request->contact);
        $contact->addresses; 
        return view('address-list',compact('contact'));       
    }
}
