@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <h2>Add New Phone</h2>
                        <a class="btn ml-auto" href="{{ route('contact',[$contact->id]) }}">Back</a>
                    </div>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success text-capitalize text-center" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if (session('failed'))
                        <div class="alert alert-danger text-capitalize text-center" role="alert">
                            {{ session('failed') }}
                        </div>
                    @endif
                </div>
                <div class="row card-body mt-3">
                    <form class="form-group col-8 mx-auto" action="{{ route('phone_store') }}" method="POST">
                        @csrf
                        <input type="hidden" name="id" value="{{ isset($phone)? $phone->id : null }}">
                        <input type="hidden" name="contact_id" value="{{ isset($contact)? $contact->id : null }}">
                        <div class="form-group row">
                            <label for="name" class="col-sm-4 col-form-label text-capitalize">Address Name</label>
                            <div class="col-sm-8">
                                <select class="form-control @error('name') is-invalid @enderror" name="name" id="name" value="{{ isset($phone)? $phone->name : old('name') }}" name="name" id="name">
                                    <option value="land">Land</option>
                                    <option value="mobile">Mobile</option>
                                    <option value="office">Office</option>
                                    <option value="day">Day</option>
                                    <option value="other">Other</option>
                                </select>
                            </div>
                            @error('name')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group row">
                            <label for="number" class="col-sm-4 col-form-label text-capitalize">Number</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control @error('number') is-invalid @enderror" name="number" id="number" value="{{ isset($phone)? $phone->number : old('number') }}" placeholder="enter phone number">
                            </div>
                            @error('number')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group row">
                            <button class="btn btn-outline-primary" type="submit">Add phone</button>
                        </div>
                    </form>
                </div>
                <div class="card-body col-8 mx-auto">
                    <h3 class="text-center">{{ $contact->first_name }}'s Phone List</h3>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Number</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($contact->phones as $phone)
                            <tr>
                                <td>{{ $phone->name }}</td>
                                <td>{{ $phone->number }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
