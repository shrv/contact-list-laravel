@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h2>Dashboard</h2>
                </div>

                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th scope="col">First Name</th>
                                <th scope="col">Last Name</th>
                                <th scope="col">Email</th>
                                <th scope="col" colspan="4"><a class="btn" href="{{ route('contact') }}">Add New</a></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($contacts as $contact)
                            <tr>
                                <td>{{ $contact->first_name }}</td>
                                <td>{{ $contact->last_name }}</td>
                                <td>{{ $contact->email }}</td>
                                <td><a  class="btn" href="{{ route('contact',[$contact->id]) }}">Edit</a></td>
                                <td><a  class="btn" href="{{ route('delete',[$contact->id]) }}">Delete</a></td>
                                <td><a  class="btn" href="{{ route('address',[$contact->id]) }}">Address List</a></td>
                                <td><a  class="btn" href="{{ route('phone',[$contact->id]) }}">Phone List</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
