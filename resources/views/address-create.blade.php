@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <h2>Add New Address</h2>
                        <a class="btn ml-auto" href="{{ route('contact',[$contact->id]) }}">Back</a>
                    </div>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success text-capitalize text-center" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if (session('failed'))
                        <div class="alert alert-danger text-capitalize text-center" role="alert">
                            {{ session('failed') }}
                        </div>
                    @endif
                </div>
                <div class="row card-body mt-3">
                    <form class="form-group col-8 mx-auto" action="{{ route('address_store') }}" method="POST">
                        @csrf
                        <input type="hidden" name="id" value="{{ isset($address)? $address->id : null }}">
                        <input type="hidden" name="contact_id" value="{{ isset($contact)? $contact->id : null }}">
                        <div class="form-group row">
                            <label for="name" class="col-sm-4 col-form-label text-capitalize">Address Name</label>
                            <div class="col-sm-8">
                                <select class="form-control @error('name') is-invalid @enderror" name="name" id="name" value="{{ isset($address)? $address->name : old('name') }}" name="name" id="name">
                                    <option value="home">Home</option>
                                    <option value="home">Office</option>
                                    <option value="other">Other</option>
                                </select>
                            </div>
                            @error('name')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group row">
                            <label for="street" class="col-sm-4 col-form-label text-capitalize">Street</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control @error('street') is-invalid @enderror" name="street" id="street" value="{{ isset($address)? $address->street : old('street') }}" placeholder="street">
                            </div>
                            @error('street')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group row">
                            <label for="city" class="col-sm-4 col-form-label text-capitalize">City Address</label>
                            <div class="col-sm-8">
                                <input type="city" class="form-control @error('city') is-invalid @enderror" name="city"  id="city" value="{{ isset($address)? $address->city : old('city') }}" placeholder="city Address">
                            </div>
                            @error('city')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group row">
                            <label for="state" class="col-sm-4 col-form-label text-capitalize">state Address</label>
                            <div class="col-sm-8">
                                <input type="state" class="form-control @error('state') is-invalid @enderror" name="state"  id="state" value="{{ isset($address)? $address->state : old('state') }}" placeholder="state Address">
                            </div>
                            @error('state')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group row">
                            <label for="zip_po" class="col-sm-4 col-form-label text-capitalize">Zip/Po Address</label>
                            <div class="col-sm-8">
                                <input type="zip_po" class="form-control @error('zip_po') is-invalid @enderror" name="zip_po"  id="zip_po" value="{{ isset($address)? $address->zip_po : old('zip_po') }}" placeholder="Zip/Po Address">
                            </div>
                            @error('zip_po')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group row">
                            <label for="country" class="col-sm-4 col-form-label text-capitalize">country Address</label>
                            <div class="col-sm-8">
                                <input type="country" class="form-control @error('country') is-invalid @enderror" name="country"  id="country" value="{{ isset($address)? $address->country : old('country') }}" placeholder="country name">
                            </div>
                            @error('country')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group row">
                            <button class="btn btn-outline-primary" type="submit">Add address</button>
                        </div>
                    </form>
                </div>
                <div class="card-body col-8 mx-auto">
                    <h3 class="text-center">{{ $contact->first_name }}'s Address List</h3>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Street</th>
                                <th scope="col">City</th>
                                <th scope="col">State</th>
                                <th scope="col">Zip/Po</th>
                                <th scope="col">Country</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($contact->addresses as $address)
                            <tr>
                                <td>{{ $address->name }}</td>
                                <td>{{ $address->street }}</td>
                                <td>{{ $address->city }}</td>
                                <td>{{ $address->state }}</td>
                                <td>{{ $address->zip_po }}</td>
                                <td>{{ $address->country }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
