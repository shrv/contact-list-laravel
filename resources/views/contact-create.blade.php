@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <h2>{{ isset($contact)? $contact->first_name."'s Contact Details" : 'Add New Contact' }}</h2>
                        <a class="btn ml-auto" href="{{ route('home') }}">Back</a>
                    </div>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success text-capitalize text-center" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if (session('failed'))
                        <div class="alert alert-danger text-capitalize text-center" role="alert">
                            {{ session('failed') }}
                        </div>
                    @endif
                </div>
                <div class="row card-body mt-3">
                    <form class="form-group col-8 mx-auto" action="{{ route('contact_store') }}" method="POST">
                        @csrf
                        <input type="hidden" name="id" value="{{ isset($contact)? $contact->id : null }}">
                        <div class="form-group row">
                            <label for="first_name" class="col-sm-4 col-form-label text-capitalize">First Name</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control @error('first_name') is-invalid @enderror" name="first_name" id="first_name" value="{{ isset($contact)? $contact->first_name : old('fast_name') }}" placeholder="enter first name">
                            </div>
                            @error('first_name')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group row">
                            <label for="last_name" class="col-sm-4 col-form-label text-capitalize">Last Name</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control @error('last_name') is-invalid @enderror" name="last_name" id="last_name" value="{{ isset($contact)? $contact->last_name : old('last_name') }}" placeholder="enter last name">
                            </div>
                            @error('last_name')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group row">
                            <label for="email" class="col-sm-4 col-form-label text-capitalize">Email Address</label>
                            <div class="col-sm-8">
                                <input type="email" class="form-control @error('email') is-invalid @enderror" name="email"  id="email" value="{{ isset($contact)? $contact->email : old('email') }}" placeholder="email@example.com">
                            </div>
                            @error('email')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group row">
                            <label for="date_of_birth" class="col-sm-4 col-form-label text-capitalize">Date of Birth</label>
                            <div class="col-sm-8">
                                <input type="date" class="form-control @error('date_of_birth') is-invalid @enderror" name="date_of_birth" id="date_of_birth" value="{{ isset($contact)? $contact->date_of_birth : old('date_of_birth') }}">
                            </div>
                            @error('date_of_birth')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        @if($contact)
                        <div class="form-group row">
                            <div class="mx-auto">
                                <a class="btn btn-outline-primary" href="{{ url('contacts/addresses/'.$contact->id) }}">Add Address</a>
                                <a class="btn btn-outline-primary" href="{{ url('contacts/phones/'.$contact->id) }}">Add Phone</a>
                            </div>
                        </div>
                        @endif
                        <div class="form-group row">
                            <button class="btn btn-outline-primary" type="submit">Add Contact</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
